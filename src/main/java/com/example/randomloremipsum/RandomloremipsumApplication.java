package com.example.randomloremipsum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandomloremipsumApplication {

    public static void main(String[] args) {
        SpringApplication.run(RandomloremipsumApplication.class, args);
    }
}
