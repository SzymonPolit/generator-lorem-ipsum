package com.example.randomloremipsum.api;

import de.svenjacobs.loremipsum.LoremIpsum;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class HomeController {

    private LoremIpsum loremIpsum = new LoremIpsum();


    @GetMapping("/")
    public String createdValue(){
        return "index";
    }

    @GetMapping("/generated")
    public String createNewLoremIpsum (@RequestParam String amount, ModelMap map) {
        Integer word = Integer.valueOf(amount); //amount to jest wartość wpisywana w guziku
        map.put("wordsloremipsum", loremIpsum.getWords(word));
        return "generatedloremipsum";
    }
}
